<?php


class Chees {
	
	public function isCorrectField(string $field) {
		if (strlen($field) != 2) {
			return false;
		}
		
		$x = $this->getX($field);
		$y = $this->getY($field);
		
		if (!is_int($x) || !$this->isCordOnBoard($x)) {
			return false;
		}
		
		if (!is_int($y) || !$this->isCordOnBoard($y)) {
			return false;
		}
		
		return true;
	}
	
	private function isCordOnBoard(int $cord) {
		if ($cord > 8) {
			return false;
		}
		
		if ($cord < 1) {
			return false;
		}
		
		return true;
	}
	
	private function getX(string $field) {
		return (int) substr($field, 0, 1);
	}
	
	private function getY(string $field) {
		return (int) substr($field, 1, 1);
	}
	
	
	public function getMoveCount(string $field) {
		if (!$this->isCorrectField($field)) {
			return 0;
		}
		
		$x = $this->getX($field);
		$y = $this->getY($field);
		$count = 0;
		
		$dx = -2;
		while ($dx <= 2) {
			
			if ($dx == 0) {
				$dx++;
				continue;
			}
			
			
			$dy = -2;
			while($dy <= 2) {
				
				if ($dy == 0) {
					$dy++;
					continue;
				}
				
				if (abs($dy) == abs($dx)) {
					$dy++;
					continue;
				}
				
				$newX = $x + $dx;
				$newY = $y + $dy;
				
				if ($this->isCordOnBoard($newX) && $this->isCordOnBoard($newY)) {
					echo "Cord $newX $newY is good" . PHP_EOL;
					$count++;
				}
				
				$dy++;
			}
			
			$dx++;
		}
		
		return $count;
	}
}

$chees = new Chees();

echo $chees->getMoveCount('77');
